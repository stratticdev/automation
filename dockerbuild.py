#!/usr/bin/env python3.5
# -*- coding: utf-8 -*-

import sys
sys.path.append('/usr/local/lib/')
import argparse
import docker
from mgpylib import gsdata as stratticdata
import subprocess
from urllib.parse import urlparse
from mgpylib.color import TerminalColor as color


def main():
	parser = argparse.ArgumentParser(description='Build (and run) Dockerfile')
	parser.add_argument('client' , nargs='*', help='One or more client ID')
	parser.add_argument('--dockerfile', nargs='?',default='_Docker.install', help="Specify a docker file to use (development)")
	parser.add_argument('--tag', nargs='?',default='production', help='Tag an image. (default :production)')
	parser.add_argument('--build', help='Build new container ',action='store_true')
	parser.add_argument('--run', help='Run new container ',action='store_true')
	parser.add_argument('--dev', help='Build a dev site container',action="store_const",const="dev", default="client",dest="userange")
	args = parser.parse_args()
	client_found = False

	if args.client:
		pubclient = args.client
	else:
		print(color.RED+"No client specified. Exiting."+color.END)
		parser.print_help()
		exit()

	tag = args.tag

	if not args.build and not args.run:
		print(color.RED+"Choose --build, --run or both."+color.END)
		exit()
	try:
		for client in stratticdata.main(showcolumns=['cfurl','cfid','stagingurl','stagingpwd','stagingip','clientname','s3bucket','docker_network'],dataformat='list',userange=args.userange):
			if client[5] in pubclient:
				client_found = True
				print(color.BLUE+"Fetching information for "+client[5]+color.END)
				docker_conn = docker.from_env()
				try:
					# URLs are inconsistent in the spreadsheet. We strip protocols from homeurl, just to add them back in the sed statement of the Dockerfile.
					cfurl = client[0]
					cfid = client[1]
					homeurl = urlparse(client[2]).netloc
					genpass = client[3]
					ipaddr = client[4]
					clientname = client[5]
					s3bucket = client[6]
					dockernet = client[7]

				except Exception as e:
					print(e)
				try:
					dcont = docker_conn.containers.get(client[5])
					print(color.BLUE+"Current status of container: "+dcont.status+color.END)
					has_container = True
				except docker.errors.NotFound as fnf:
					print(color.YELLOW+"No container for "+client[5]+color.END)
					has_container = False
					pass

				try:
					if args.build is True:
						dockerfile = args.dockerfile
						dockerbuild = "docker build --build-arg homeurl="+homeurl+" --build-arg S3bucket="+s3bucket+" --build-arg client="+clientname+" --build-arg CFID="+cfid+" --build-arg CFUrl="+cfurl+" --build-arg genpass="+genpass+" -t "+clientname+":"+tag+" -f /images/"+dockerfile+" /images/"
						print(dockerbuild)
						process=subprocess.Popen([dockerbuild], stdout=subprocess.PIPE,stderr=subprocess.STDOUT, shell=True)
						while True:
							output = process.stdout.readline().decode('utf-8')
							if output == '' and process.poll() is not None:
								break
							if output:
								print(str(output.strip()))
						rc = process.poll()
						print(color.GREEN+"Docker image for "+clientname+" successfully created"+color.END)
				except Exception as e:
					print(e)

				try:
					if args.run is True:
						#-m 4g
						#--cpuset-cpus="1,3"
						dockerrun = "docker run -dit --restart unless-stopped --tmpfs /tmp:rw,noexec,nosuid -v "+client[5].capitalize()+"HTML:/var/www/html -v "+client[5].capitalize()+"Mysql:/var/lib/mysql --hostname="+client[5]+" --net "+client[7]+" --ip "+client[4]+" --name "+client[5]+" "+client[5]+":"+tag
						print(dockerrun)
						if has_container:
							dcont = docker_conn.containers.get(client[5])
							print(dcont.status)
							if dcont.status == "exited":
								dockerrun = "docker start "+clientname
								process=subprocess.Popen([dockerrun], stdout=subprocess.PIPE,stderr=subprocess.STDOUT, shell=True).communicate()[0]
								print(color.GREEN+"Container for existing image started"+color.END)
							elif dcont.status == "created":
								process=subprocess.Popen([dockerrun], stdout=subprocess.PIPE,stderr=subprocess.STDOUT, shell=True).communicate()[0]
								print(process)
								print(color.GREEN+"Docker container "+clientname+" successfully started."+color.END)
								
						else:
							process=subprocess.Popen([dockerrun], stdout=subprocess.PIPE,stderr=subprocess.STDOUT, shell=True).communicate()[0]
							print(process)
							print(color.GREEN+"Docker container "+clientname+" successfully running."+color.END)
				except Exception as e:
					print(color.RED+client[0]+str(e)+color.END)
		if client_found is not True:
			print(color.RED+"Client "+", ".join(pubclient)+" not found"+color.END)

	except TypeError as e:
		print(e)
		print("Error")

if __name__ == "__main__":
	main()
