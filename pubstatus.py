#!/usr/bin/env python3.5
# -*- coding: utf-8 -*-

import docker
import argparse

client = docker.from_env()

parser = argparse.ArgumentParser(description='Process deployment request.')
parser.add_argument('--exitcode','-x' , action='store_true', help='Send exit code')
args = parser.parse_args()

for dcont in client.containers.list():
	res = dcont.exec_run("cat /tmp/deploy.pid")
	if args.exitcode is True:
		print(res.exit_code)
	else:
		if res.exit_code > 0:
			print(dcont.name+" not publishing")
			#res = dcont.exec_run("tail -n10 /tmp/log")
			#print(res.output.decode('utf-8'))
		else:
			print(dcont.name+" currently publishing")
			#res = dcont.exec_run("head -n10 /tmp/log")
			#print(res.output.decode('utf-8'))
