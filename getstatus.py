#!/usr/bin/env python3.5

import sys
sys.path.append('/usr/local/lib')
import argparse
from mgpylib import gsdata as stratticdata
import requests
from requests.auth import HTTPBasicAuth
from mgpylib.color import TerminalColor as color

rcode = {200:color.GREEN,404:color.RED,401:color.MAGENTA,500:color.YELLOW,502:color.ORANGE}
parser = argparse.ArgumentParser(description='Process deployment request.')
parser.add_argument('--site','-s' , choices=['stagingurl','cfurl'] , type=str, default='stagingurl', help='Set to cf')
parser.add_argument('--dev', help='Build a dev site container',action="store_const",const="dev", default="client",dest="userange")
args = parser.parse_args()

sites = "Cloudfront" if args.site == "cfurl" else "Staging"

print(color.BLUE+"Getting status for "+sites+" sites"+color.END)
try:
	for client in stratticdata.main(showcolumns=['name',args.site,'stagingpwd'],dataformat='list',userange=args.userange):
		if client[1] !='':
			try:
				clienturl = client[1]
				if not clienturl.startswith("https://"):
					clienturl = "https://"+client[1]
				resp = requests.get(clienturl, auth=HTTPBasicAuth('strattic', client[2]),verify='/etc/ssl/certs/')
				print(rcode[resp.status_code]+str(resp.status_code)+": "+client[0]+": "+client[1]+color.END)
			except requests.exceptions.ConnectionError as e:
				print(color.RED+str(resp.status_code)+": "+client[0]+" Connection error. Check URL:"+client[1]+color.END)
				#print(e)
except TypeError as e:
	print(e)
