#!/usr/bin/env python3.5
# -*- coding: utf-8 -*-

import sys
sys.path.append('/usr/local/lib/')
import docker
import time
import argparse
import os
import re
import shutil
import subprocess
from datetime import datetime
from urllib.parse import urlparse
from mgpylib.color import TerminalColor as color
from mgpylib import gsdata as stratticdata

# globals
Strattic_Debug = True
log = '/tmp/deploy-plugin.log'
plugindir = '/usr/local/bin/strattic-publish/'
limit_to = []
# Deletes a directory tree.
# Function similar to rm -rf <path>
#
# @param string  path   The new directory path
# @param bool ignerr	Boolean value whether to ignore errors
def rmtree(path,ignerr):
	shutil.rmtree(path,ignore_errors=ignerr)
	
# Debug logger.
# Outputs debug information to both the console and to a logfile.
#
# @param string  statement   The statement to out to the debug log
# TODO: change to loggging module
def Debug(statement):
	global log, Strattic_Debug
	timestp = str(datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d-%H-%M-%S'))
	if Strattic_Debug is True :
		print("["+timestp+"] "+statement)
	ansi_escape = re.compile(r'\x1b[^m]*m')
	statement = ansi_escape.sub('', statement)
	with open(log,'a') as logfile:
		logfile.write("["+timestp+"] "+statement+"\n")
def get_config_data():
	for client in stratticdata.main(showcolumns=['cfurl','cfid','stagingurl','stagingpwd','clientname','s3bucket'],dataformat='list',userange=args.userange):
		if client[4] in limit_to:
			return client

def main():
	global args,limit_to
	# init
	if os.path.isfile(log):
		os.remove(log)	

	#get command line arguments
	parser = argparse.ArgumentParser(description='Process plugin upgrade deployment.')
	parser.add_argument('--verbose','-v' , action='store_false', help='Output results')
	parser.add_argument('--config','-c' , action='store_true', help='Update configuration file')
	parser.add_argument('clients', nargs='*', help='Only update the clients listed')
	parser.add_argument('--dev', help='Build a dev site container',action="store_const",const="dev", default="client",dest="userange")
	args = parser.parse_args()
	# Sets more variables for code readability
	if args.verbose is True:
		Strattic_Debug = True
	if args.clients:
		limit_to = args.clients
		Debug("Only updating "+", ".join(limit_to))
	#When we're ready, we can eliminate this else statement:
	else:
		Strattic_Debug = True
		Debug(color.RED+"Global deployment not implemented. Please specify client(s) for plugin deployment."+color.END)
		parser.print_help()
		exit()

	if not args.config:
		deploy_config = False
	else:
		deploy_config = args.config

	# Create a connection to the docker daemon
	docker_conn = docker.from_env()
	container_exists = False

	try:
		# Loops through the list of running docker containers on the server
		# Future: spin up each docker container to update plugin, then spin down. Or, mark for future upgrade when customer spins up next.
		for dcont in docker_conn.containers.list():
			try:
				# Matches against the list of clients supplied, or if none, all of the listed clients
				if any(dcont.name in s for s in limit_to) or args.clients is None:
					container_exists = True
				
					#Returns 1 (no file) or 0 (file exists)
					res = dcont.exec_run("cat /tmp/deploy.pid")
					# No pid file exists
					if res.exit_code > 0:
						Debug(dcont.name+" not publishing")
						try:
							if(os.path.isfile(plugindir+"wordpress-plugin/strattic-config.php") and not deploy_config):
								#Move the default config file out of the the way so it doesn't overwrite the existing config file
								shutil.move(plugindir+"wordpress-plugin/strattic-config.php", "/tmp/strattic-config.php")
								Debug(color.RED+"Config file not deployed."+color.END)
							
							#Check if the necessary python libraries are installed, and install them if missing. Eventually this will become unneeded.
							checkinstalls = "/usr/bin/docker exec "+dcont.short_id+" sh -c \"dpkg -l | egrep 'python3-(boto3|redis|httplib2)'\""
							has_libs=subprocess.Popen([checkinstalls],stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True).communicate()[0]
							if not has_libs:
								Debug(color.GREEN+"Installing python libs"+color.END)
								pyupd = "/usr/bin/docker exec "+dcont.short_id+" sh -c \"/usr/bin/apt-get update && apt-get install -y python3-boto3 python3-httplib2 python3-redis\""
								res=subprocess.Popen([pyupd],stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True).communicate()[0]
							#Copy plugin dir into the container
							res=subprocess.Popen(["/usr/bin/docker cp "+plugindir+". "+dcont.short_id+":"+plugindir], stdout=subprocess.PIPE,stderr=subprocess.STDOUT, shell=True).communicate()[0]
							Debug(plugindir+" copied into container.")
							#If we moved the config file, put it back for the next time
							if(os.path.isfile("/tmp/strattic-config.php")):
								shutil.move("/tmp/strattic-config.php", plugindir+"wordpress-plugin/strattic-config.php")
						except Exception as e:
							Debug("Folder not copied into container.")
							Debug(str(e))
							exit()
						try:
							#If we need/want to update the strattic-config file
							if deploy_config is True:
								Debug("Updating configuration file in container")
								configdata = get_config_data()
								rootdir ="\/usr\/local\/bin\/strattic-publish\/"
								plugdir ="\/usr\/local\/bin\/strattic-publish\/wordpress-plugin\/"
								#Identical to dockerfile
								sedcommand = "/usr/bin/docker exec "+dcont.short_id+" sh -c \"sed -i -e 's/\$wordpress_plugin_dir/"+plugdir+"/g' -e 's/\$home_url/https:\/\/"+urlparse(configdata[2]).netloc+"/g' -e 's/\$cloudfront_url/https:\/\/"+configdata[0]+"/g' -e 's/\$s3_bucket/"+configdata[5]+"/g' -e 's/\$cloudfront_id/"+configdata[1]+"/g' -e 's/\$password/"+configdata[3]+"/g' -e 's/\$user/strattic/g' -e 's/\$directory/"+rootdir+"/g' /usr/local/bin/strattic-publish/wordpress-plugin/strattic-config.php\" "
								res=subprocess.Popen([sedcommand],stdout=subprocess.PIPE,stderr=subprocess.STDOUT, shell=True).communicate()[0]
						except Exception as e:
							Debug("Updating configuration file failed")
							print(str(e))
							exit()

						try:
							#Make an mu-plugins directory if none exists
							res=subprocess.Popen(["/usr/bin/docker exec "+dcont.short_id+" sh -c \"mkdir -p /var/www/html/wp-content/mu-plugins\" "],stdout=subprocess.PIPE,stderr=subprocess.STDOUT, shell=True).communicate()[0]
							#Copy the files into the mu-plugins directory
							res=subprocess.Popen(["/usr/bin/docker exec "+dcont.short_id+" sh -c \"cp -r "+plugindir+"wordpress-plugin/mu-plugins/* /var/www/html/wp-content/mu-plugins/\" "],stdout=subprocess.PIPE,stderr=subprocess.STDOUT, shell=True).communicate()[0]
							#Maintenance
							res = dcont.exec_run("chown -R www-data: /var/www/html/wp-content/")
							res = dcont.exec_run("chown -R www-data: /usr/local/bin/strattic-publish/")
							Debug("MU plugin files moved into /var/www/html/wp-content/mu-plugins/[strattic]")
						except Exception as e:
							Debug("MU files not moved to /var/www/html/wp-content/mu-plugins/strattic/")
							print(str(e))
							exit()
					else:
						Debug(dcont.name+" currently publishing. Skipped")
			except Exception as e:
				print("Error 2")
				print(str(e))
				exit()
		if not container_exists:
			Debug("The container(s) you requested could not be found. Please check your spelling.")
	except Exception as e:
		print("Error 1")
		print(e)

if __name__ == '__main__':
	main()
