#!/bin/bash

POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    -c|--client)
    client="$2"
    Client="${client^}"
    shift # past argument
    shift # past value
    ;;
    -a|--action)
    action="$2"
    shift # past argument
    shift # past value
    ;;
    -p|--port)
    sftpport="$2"
    shift # past argument
    shift # past value
    ;;
    -i|--ip)
    ipaddress="$2"
    shift
    shift
    ;;
    --disable)
    action="disable"
    shift # past argument
    ;;
    --enable)
    action="enable"
    shift
    ;;
    *)    # unknown option
    POSITIONAL+=("$1") # save it in an array for later
    shift # past argument
    ;;
esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters


if [[ -z ${client} ]]; then
	echo "No client selected. use -c <client>"
	exit 2
fi

if [[ -z ${ipaddress} ]]; then
	ipaddress=$(docker ps -q | xargs docker inspect --format '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}   {{.Name}}' | grep ${client} | awk '{print $1}')
fi

inuse=$(docker ps  | grep 2222 | awk '{print $NF}')
#action="$2"
echo $inuse

if [[ -z ${ipaddress} ]]; then
	echo "Container for ${client} wasn't already running. Manually run docker first. (We can automate when we can get the IP address)"
	exit 3
elif [ ! -z ${inuse} ] && [ $action = "enable" ]; then
	echo "Port in use by ${inuse}. TODO autodisable (check recursive)"
	exit 4
else
	echo good to go > /dev/null
fi

if [[ -z ${action} ]]; then
	echo "No action specfied. Enabling"
	action=enable
fi

if [ -z ${port} ] && [ "${action}"="enable" ]; then
	echo "Setting port to 2222"
	sftpport=2222
fi


echo "Setting SFTP to ${action}d for ${client} (${ipaddress})"

#sftpport=$(( ( RANDOM % 100 )  + 2200 ))
#sftpport=2222
OCID=`/usr/bin/docker ps | grep ${client} | awk '{print $1}'`
OIID=`/usr/bin/docker ps | grep ${client} | awk '{print $2}'`
/usr/bin/docker stop ${client}
/usr/bin/docker rm ${client}
echo "Docker image Stopped"

if [[ $(/usr/bin/docker container inspect ${OCID} | jq -cr [.[0].HostConfig.AutoRemove] | tr -d "\[\]") -ne "true" ]]; then
/usr/bin/docker rm ${OCID}
fi
echo "removed"


if [[ "${action}" = "disable" ]]; then
	port=""
	echo "Disabling port forwarding"
else
	port="-p 0.0.0.0:${sftpport}:22"
	echo "Enabling port forwarding"
fi

CID=`/usr/bin/docker run ${port} -dit --rm --tmpfs /tmp:rw,noexec,nosuid,size=2097152k -v ${Client}HTML:/var/www/html -v ${Client}Mysql:/var/lib/mysql --hostname=${client} --net stratnet --ip ${ipaddress} --name ${client} ${OIID}`
#echo "CID ${CID}"

if [[ -n $port ]]; then
	/usr/bin/docker exec -d ${CID} sh -c "service ssh start"
	echo "ssh started"
	/usr/bin/docker exec -d ${CID} sh -c "/usr/bin/chsh -s /bin/bash www-data"
	echo "shell has been changed for www-data"
	#echo "Enter password for www-data user:"
	read -s -p "Enter the password for www-data: " somepassword
	echo
	echo "the password is $somepassword"
	/usr/bin/docker exec -d ${CID} sh -c "(echo ${somepassword}; echo ${somepassword}) | passwd www-data"
fi
#/usr/bin/docker ps | grep $RAND | awk '{print NF}'
echo "Docker container running"
exit
