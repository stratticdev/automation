#!/bin/bash

client=$1
Client="${client^}"
ipaddress=$2
region="us-east-1"
nginxpath="/etc/nginx/sites-available/"
supportemail="marc@strattic.com"

if [[ -z ${client} ]]; then
	echo "No client name provided"
	exit 2
fi
if [[ -z ${ipaddress} ]]; then
	echo "No IP address provided"
	exit 3
fi


echo "Begin Provisioning client ${client} (${Client})"
echo "IP is $ipaddress"

if [[ $ipaddress == 10.100.100.* ]]; then 
dockernet="intranet"
else
dockernet="stratnet"
fi
#
# Nginx
#
echo "Nginx Configuration"
rm $nginxpath${client}
cp $nginxpath"_skel" $nginxpath${client}
sed -i -e "s/{{client}}/${client}/g" -e "s/{{Client}}/${Client}/g" -e "s/{{ip_address}}/${ipaddress}/g" $nginxpath${client}
unlink /etc/nginx/sites-enabled/${client}
ln -s $nginxpath$1 /etc/nginx/sites-enabled/$1
genpass=`/usr/local/bin/GeneratePassword`
echo "Generated Password:"
echo $genpass
htpasswd -b -c "/etc/nginx/.${Client}Htpasswd" strattic $genpass

echo "mysql for CRM: UPDATE table SET genpwd=\"$genpass\" WHERE client = ${client}" 

#
# AWS
#
echo "AWS S3 Bucket Creation"
/usr/local/bin/aws --color on --output table --profile marc --no-verify-ssl s3api create-bucket --bucket www.${client}.com --acl public-read
/usr/local/bin/aws --color on --output table --profile marc --no-verify-ssl s3api put-bucket-website --bucket www.${client}.com --website-configuration '{"IndexDocument":{"Suffix": "index.html"},"ErrorDocument":{"Key":"404.html"}}'
/usr/local/bin/aws --color on --output table --profile marc --no-verify-ssl s3api put-bucket-policy --bucket www.${client}.com --policy '{"Version":"2008-10-17","Statement":[{"Sid":"AllowPublicRead","Effect":"Allow","Principal":{"AWS":"*"},"Action":"s3:GetObject","Resource":"arn:aws:s3:::www.'${client}'.com/*"}]}'

echo "AWS Cloudfront Provisioning"
/usr/local/bin/aws --profile marc --no-verify-ssl cloudfront create-distribution --origin-domain-name www.${client}.com.s3.amazonaws.com --default-root-object index.html > ${client}.cloudfront.json
ETag=`/usr/bin/jq '.ETag' ${client}.cloudfront.json | sed -e "s/\"//g" `
CFID=`/usr/bin/jq '.Distribution.Id' ${client}.cloudfront.json | sed -e "s/\"//g" `

echo "ETag: $ETag"
echo "CFId: $CFID"
echo "Getting Distribution configuration"
unlink ${client}.cf.gdc.json
/usr/local/bin/aws --profile marc --no-verify-ssl cloudfront get-distribution-config --id ${CFID} > ${client}.cf.gdc.json
#Add Headers Lambda
echo "Adding Modified Headers Lambda"
/usr/bin/jq .DistributionConfig.DefaultCacheBehavior.LambdaFunctionAssociations.Quantity=1 ${client}.cf.gdc.json > ${client}.cf.gdc.json.tmp && mv ${client}.cf.gdc.json.tmp ${client}.cf.gdc.json
/usr/bin/jq .DistributionConfig.DefaultCacheBehavior.LambdaFunctionAssociations.Items[0]='{"LambdaFunctionARN":"arn:aws:lambda:us-east-1:947567659378:function:ModifyResponseHeaders:7","EventType":"viewer-response"}' ${client}.cf.gdc.json > ${client}.cf.gdc.json.tmp && mv ${client}.cf.gdc.json.tmp ${client}.cf.gdc.json
#Redirect everything to https
echo "Redirecting to https"
/usr/bin/jq '.DistributionConfig.DefaultCacheBehavior.ViewerProtocolPolicy="redirect-to-https"' ${client}.cf.gdc.json > ${client}.cf.gdc.json.tmp && mv ${client}.cf.gdc.json.tmp ${client}.cf.gdc.json
#Enable Compression
echo "Enabling Compression"
/usr/bin/jq  .DistributionConfig.DefaultCacheBehavior.Compress=true ${client}.cf.gdc.json > ${client}.cf.gdc.json.tmp && mv ${client}.cf.gdc.json.tmp ${client}.cf.gdc.json
#Add CNAME Aliases
echo "Adding CNAMES"
/usr/bin/jq .DistributionConfig.Aliases.Quantity=2 ${client}.cf.gdc.json > ${client}.cf.gdc.json.tmp && mv ${client}.cf.gdc.json.tmp ${client}.cf.gdc.json
/usr/bin/jq .DistributionConfig.Aliases.Items=[\"${client}.com\",\"www.${client}.com\"] ${client}.cf.gdc.json > ${client}.cf.gdc.json.tmp && mv ${client}.cf.gdc.json.tmp ${client}.cf.gdc.json

#Very important! Have to pop it out of its root JSON container
echo "Fixing the JSON"
/usr/bin/jq .=.DistributionConfig ${client}.cf.gdc.json > ${client}.cf.gdc.json.tmp && mv ${client}.cf.gdc.json.tmp ${client}.cf.gdc.json
#And finally send it all back to AWS
echo "Sending Config to AWS"
/usr/local/bin/aws --color on --output table --profile marc --no-verify-ssl cloudfront update-distribution --id ${CFID} --if-match ${ETag} --distribution-config file://${client}.cf.gdc.json
#Get the Cloudfront Domain Name
echo "Getting CloudFront domain name"
CFdomain=`aws --profile marc cloudfront get-distribution --id ${CFID} | jq -s '.[0].Distribution.DomainName' | sed -e 's/"//g'`
#
# Docker 
#
echo "Docker Provisioning"
# Create volumes
/usr/bin/docker volume create ${Client}HTML
/usr/bin/docker volume create ${Client}Mysql
# Create Client Dockerfile
echo "Dockerfile Creation"
sed   -e "s/{{genpass}}/${genpass}/g" -e "s/{{client}}/${client}/g" -e "s/{{Client}}/${Client}/g" -e "s/{{ip_address}}/${ipaddress}/g" -e "s/{{CFdomain}}/${CFdomain}/g" -e "s/{{CFID}}/${CFID}/g" /images/_Docker > /images/${Client}Docker
# Build container and deploy
echo "Building Docker Container"
/usr/bin/docker build -q --build-arg domain=${client}.stratticstage.com -t ${client} -f /images/${Client}Docker /images/
echo "Container Built"
/usr/bin/docker run -tid -v ${Client}HTML:/var/www/html -v ${Client}Mysql:/var/lib/mysql  --hostname=${client} --net $dockernet --ip ${ipaddress} ${client}
echo "Container Started"
dockercontainer=`docker ps | grep ${client} | awk {'print $1'}`
# Rename the docker container
/usr/bin/docker rename ${dockercontainer} ${client}

#
# Vanilla WordPress installation in the container
#
echo "Setting Up WordPress in the container"
/usr/bin/docker exec -it ${client} sh -c "/usr/local/bin/wp --allow-root core download --path=/var/www/html"
/usr/bin/docker exec -it ${client} sh -c "/usr/local/bin/wp --allow-root config create --path=/var/www/html --dbname=wordpress --dbuser=wpuser --dbpass=Strattic007"
/usr/bin/docker exec -it ${client} sh -c "/usr/local/bin/wp --allow-root core install --path=/var/www/html --url=${client}.stratticstage.com --title=${Client} --admin_user=stratticdev --admin_email=support@strattic.com > /root/wpAdminPass"
/usr/bin/docker exec -it ${client} sh -c "/usr/local/bin/wp --allow-root --path=/var/www/html plugin activate strattic"
/usr/bin/docker exec -it ${client} sh -c "/bin/chown -R www-data: /var/www/html/"
/usr/bin/docker cp ${client}:/root/wpAdminPass ${client}.wpAdminPass

#echo `cat $client.wpAdminPass`
WPAdminPass=`head -n1 $client.wpAdminPass | awk {'print $3'}`
echo "UPDATE table set WPAdminPass='${WPAdminPass}' WHERE "

#
# Restart nginx
#
echo "Restarting nginx"
sudo service nginx reload

echo "Provisioning complete for ${client}"

#send an email
WPPW=`cat $client.wpAdminPass`
msg="Provisioning for ${client}.com is complete. Container ID is $dockercontainer. Generated server password is ${genpass}. ${WPPW}"
JSON=`jq -n --arg email "${supportemail}" --arg domain "$client" --arg msg "$msg" '{ msg: $msg, domain: $domain, email: $email | split(",") }'`
curl -k -H "Content-Type: application/json" -X POST -d "${JSON}" https://1mtlmyvc77.execute-api.us-east-1.amazonaws.com/PublishNotification

#
# Connect into the docker container
#
/usr/bin/docker attach ${client}
