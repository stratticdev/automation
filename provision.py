#!/usr/bin/env python3.5

import sys
sys.path.append('/usr/local/lib/')
from mgpylib.color import TerminalColor as color
from mgpylib import gsdata as stratticdata
import argparse
import json
import os
import boto3
from botocore.exceptions import ClientError
import docker
import shutil
import re
import subprocess
import time
from datetime import datetime


parser = argparse.ArgumentParser(description='Build (and run) Dockerfile')
parser.add_argument('client' , nargs='*', help='One or more client ID')
args = parser.parse_args()

# globals
Strattic_Debug = True
log = '/tmp/deploy-plugin.log'
rootdir = '/usr/local/bin/strattic-publish/'
plugindir = '/usr/local/bin/strattic-publish/wordpress-plugin/'
region="us-east-1"
nginxpath="/etc/nginx/sites-available/"
supportemail="marc@strattic.com"

# Debug logger.
# Outputs debug information to both the console and to a logfile.
#
# @param string  statement   The statement to out to the debug log
# TODO: change to loggging module
def Debug(statement):
	global log, Strattic_Debug
	timestp = str(datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d-%H-%M-%S'))
	if Strattic_Debug is True :
		print("["+timestp+"] "+statement)
	ansi_escape = re.compile(r'\x1b[^m]*m')
	statement = ansi_escape.sub('', statement)
	with open(log,'a') as logfile:
		logfile.write("["+timestp+"] "+statement+"\n")

if args.client:
	pubclient = args.client
else:
	print(color.RED+"No client specified. Exiting."+color.END)
	parser.print_help()
	exit()

def main():
	try:
		for client in stratticdata.main(showcolumns=['cfurl','cfid','stagingurl','stagingpwd','stagingip','clientname','s3bucket','docker_network'],dataformat='list'):
			if client[5] in pubclient:
				print(color.BLUE+"Fetching information for "+client[5]+color.END)
				Debug(str(client))
				try:
					Debug(color.BLUE+"Nginx Config File"+color.END)
					clientname=client[5]
					ipaddress=client[4]
					'''
					#
					# Nginx
					#
					echo "Nginx Configuration"
					sed -i -e "s/{{client}}/${client}/g" -e "s/{{Client}}/${Client}/g" -e "s/{{ip_address}}/${ipaddress}/g" $nginxpath${client}
					unlink /etc/nginx/sites-enabled/${client}
					ln -s $nginxpath$1 /etc/nginx/sites-enabled/$1
					genpass=`/usr/local/bin/GeneratePassword`
					echo "Generated Password:"
					echo $genpass
					htpasswd -b -c "/etc/nginx/.${Client}Htpasswd" strattic $genpass

					echo "mysql for CRM: UPDATE table SET genpwd=\"$genpass\" WHERE client = ${client}" 
					'''
					if os.path.isfile(nginxpath+clientname):
						os.remove(nginxpath+clientname)

					shutil.copy2(nginxpath+"_skel",nginxpath+clientname)
					with open(nginxpath+clientname,"r") as config:
						lines = []
						config = f.readlines()
						for line in config:
							if "{{client}}" in line:
								newline = line.replace("{{client}}",clientname)
								lines.append(line.replace(line,newline))
							elif "{{Client}}" in line:
								newline = line.replace("{{Client}}",clientname.capitalize())
								lines.append(line.replace(line,newline))
							elif "{{ip_address}}" in line:
								newline = line.replace("{{ip_address}}",ipaddress)
								lines.append(line.replace(line,newline))
							else:
								lines.append(line)
					with open(nginxpath+clientname,"w+") as outfile:
						for line in lines:
							outfile.write(line)

					os.symlink(nginxpath+clientname,"/etc/nginx/sites-enabled/"+clientname)

					pwd=subprocess.Popen(["/usr/local/bin/GeneratePassword"], stdout=subprocess.PIPE,stderr=subprocess.STDOUT, shell=True).communicate()[0]
					Debug("Generated password: "+pwd)

					htpwd=subprocess.Popen(["htpasswd","-b","-c","\"/etc/nginx/."+clientname+"Htpasswd\"","strattic",pwd], stdout=subprocess.PIPE,stderr=subprocess.STDOUT, shell=True).communicate()[0]
					Debug("mysql for CRM: UPDATE table SET genpwd=\"$genpass\" WHERE client = ${client}")

				except Exception as e:
					print(e)
					exit()
				
				try:
					'''
					#
					# AWS
					#
					echo "AWS S3 Bucket Creation"
					/usr/local/bin/aws --color on --output table --profile marc --no-verify-ssl s3api create-bucket --bucket www.${client}.com --acl public-read
					'''
					bucketname = "www."+clientname+".com"
					s3b = boto3.client('s3')
					try:
						s3b.head_bucket(Bucket=bucketname) #Bucket
					except ClientError as e:
						if e.response["Error"]["Code"] == 404:
							Debug("Creating new Bucket for "+clientname)
							res = s3b.create_bucket(ACL='public-read',Bucket=bucketname)
							res = s3b.put_bucket_website(Bucket=bucketname,WebsiteConfiguration='{"IndexDocument":{"Suffix": "index.html"},"ErrorDocument":{"Key":"404.html"}}')
							res = s3b.put_bucket_policy(Bucket=bucketname,Policy='{"Version":"2008-10-17","Statement":[{"Sid":"AllowPublicRead","Effect":"Allow","Principal":{"AWS":"*"},"Action":"s3:GetObject","Resource":"arn:aws:s3:::'+bucketname+'/*"}]}')

					'''
					echo "AWS Cloudfront Provisioning"
					/usr/local/bin/aws --profile marc --no-verify-ssl cloudfront create-distribution --origin-domain-name www.${client}.com.s3.amazonaws.com --default-root-object index.html > ${client}.cloudfront.json
					'''
					createdistcmd = "/usr/local/bin/aws --profile marc --no-verify-ssl cloudfront create-distribution --origin-domain-name www.${client}.com.s3.amazonaws.com --default-root-object index.html"
					cloudfront_resp = subprocess.Popen([createdistcmd],stdout=subprocess.PIPE,stderr=subprocess.STDOUT).communicate[0]
					cloudfront_json = json.loads(cloudfront_resp)

					ETag = cloudfront_json['ETag']
					CFID = cloudfront_json['Distribution']['Id']
					Debug("ETag: "+ETag)
					Debug("CFID: "+CFID)
					'''
					ETag=`/usr/bin/jq '.ETag' ${client}.cloudfront.json | sed -e "s/\"//g" `
					CFID=`/usr/bin/jq '.Distribution.Id' ${client}.cloudfront.json | sed -e "s/\"//g" `

					echo "ETag: $ETag"
					echo "CFId: $CFID"
					echo "Getting Distribution configuration"
					unlink ${client}.cf.gdc.json
					/usr/local/bin/aws --profile marc --no-verify-ssl cloudfront get-distribution-config --id ${CFID} > ${client}.cf.gdc.json
					'''
					cfr = boto3.client('cloudfront')
					cloudfront_config = cfr.get_distribution_config(Id=CFID)
					Debug(cloudfront_config)
					print(type(cloudfront_config))
					cloudfront_config["DistributionConfig"]["DefaultCacheBehavior"]["LambdaFunctionAssociations"]["Quantity"]=1
					cloudfront_config["DistributionConfig"]["DefaultCacheBehavior"]["LambdaFunctionAssociations"]["Items"][0]='{"LambdaFunctionARN":"arn:aws:lambda:us-east-1:947567659378:function:ModifyResponseHeaders:7","EventType":"viewer-response"}'
					cloudfront_config["DistributionConfig"]["DefaultCacheBehavior"]["ViewerProtocolPolicy"]="redirect-to-https"
					cloudfront_config["DistributionConfig"]["DefaultCacheBehavior"]["Compress"]=true
					cloudfront_config["DistributionConfig"]["Aliases"]["Quantity"]=2
					cloudfront_config["DistributionConfig"]["Aliases"]["Items"]=["\""+clientname+".com\",\"www."+clientname+".com\""]
					

					'''
					#Add Headers Lambda
					echo "Adding Modified Headers Lambda"
					/usr/bin/jq .DistributionConfig.DefaultCacheBehavior.LambdaFunctionAssociations.Quantity=1 ${client}.cf.gdc.json > ${client}.cf.gdc.json.tmp && mv ${client}.cf.gdc.json.tmp ${client}.cf.gdc.json
					/usr/bin/jq .DistributionConfig.DefaultCacheBehavior.LambdaFunctionAssociations.Items[0]='{"LambdaFunctionARN":"arn:aws:lambda:us-east-1:947567659378:function:ModifyResponseHeaders:7","EventType":"viewer-response"}' ${client}.cf.gdc.json > ${client}.cf.gdc.json.tmp && mv ${client}.cf.gdc.json.tmp ${client}.cf.gdc.json
					#Redirect everything to https
					echo "Redirecting to https"
					/usr/bin/jq '.DistributionConfig.DefaultCacheBehavior.ViewerProtocolPolicy="redirect-to-https"' ${client}.cf.gdc.json > ${client}.cf.gdc.json.tmp && mv ${client}.cf.gdc.json.tmp ${client}.cf.gdc.json
					#Enable Compression
					echo "Enabling Compression"
					/usr/bin/jq  .DistributionConfig.DefaultCacheBehavior.Compress=true ${client}.cf.gdc.json > ${client}.cf.gdc.json.tmp && mv ${client}.cf.gdc.json.tmp ${client}.cf.gdc.json
					#Add CNAME Aliases
					echo "Adding CNAMES"
					/usr/bin/jq .DistributionConfig.Aliases.Quantity=2 ${client}.cf.gdc.json > ${client}.cf.gdc.json.tmp && mv ${client}.cf.gdc.json.tmp ${client}.cf.gdc.json
					/usr/bin/jq .DistributionConfig.Aliases.Items=[\"${client}.com\",\"www.${client}.com\"] ${client}.cf.gdc.json > ${client}.cf.gdc.json.tmp && mv ${client}.cf.gdc.json.tmp ${client}.cf.gdc.json

					#Very important! Have to pop it out of its root JSON container
					echo "Fixing the JSON"
					/usr/bin/jq .=.DistributionConfig ${client}.cf.gdc.json > ${client}.cf.gdc.json.tmp && mv ${client}.cf.gdc.json.tmp ${client}.cf.gdc.json
					#And finally send it all back to AWS
					echo "Sending Config to AWS"
					/usr/local/bin/aws --color on --output table --profile marc --no-verify-ssl cloudfront update-distribution --id ${CFID} --if-match ${ETag} --distribution-config file://${client}.cf.gdc.json
					#Get the Cloudfront Domain Name
					echo "Getting CloudFront domain name"
					CFdomain=`aws --profile marc cloudfront get-distribution --id ${CFID} | jq -s '.[0].Distribution.DomainName' | sed -e 's/"//g'`

					'''
					cfr.update_distribution(cloudfront_config)
					cfdist = cfr.get_distribution(Id=CFID)
					CFdomain = cfdist[0]["Distribution"]["DomainName"]
					Debug(CFdomain)

				except Exception as e:
					print(e)
					exit()

				try:
					'''
					#
					# Docker 
					#
					echo "Docker Provisioning"
					# Create volumes
					/usr/bin/docker volume create ${Client}HTML
					/usr/bin/docker volume create ${Client}Mysql
					# Create Client Dockerfile
					echo "Dockerfile Creation"
					sed   -e "s/{{genpass}}/${genpass}/g" -e "s/{{client}}/${client}/g" -e "s/{{Client}}/${Client}/g" -e "s/{{ip_address}}/${ipaddress}/g" -e "s/{{CFdomain}}/${CFdomain}/g" -e "s/{{CFID}}/${CFID}/g" /images/_Docker > /images/${Client}Docker
					# Build container and deploy
					echo "Building Docker Container"
					/usr/bin/docker build -q --build-arg domain=${client}.stratticstage.com -t ${client} -f /images/${Client}Docker /images/
					echo "Container Built"
					/usr/bin/docker run -tid -v ${Client}HTML:/var/www/html -v ${Client}Mysql:/var/lib/mysql  --hostname=${client} --net $dockernet --ip ${ipaddress} ${client}
					echo "Container Started"
					dockercontainer=`docker ps | grep ${client} | awk {'print $1'}`
					# Rename the docker container
					/usr/bin/docker rename ${dockercontainer} ${client}


					'''
					docker_conn = docker.from_env()

					dvolHTML = docker_conn.volumes.create_volume(name=clientname.capitalize()+"HTML")
					dvolMysql = docker_conn.volumes.create_volume(name=clientname.capitalize()+"Mysql")

					#Use dockerbuild --run here.

				except Exception as e:
					print(e)
					exit()

				try:
					'''
					#
					# Vanilla WordPress installation in the container
					#
					echo "Setting Up WordPress in the container"
					/usr/bin/docker exec -it ${client} sh -c "/usr/local/bin/wp --allow-root core download --path=/var/www/html"
					/usr/bin/docker exec -it ${client} sh -c "/usr/local/bin/wp --allow-root config create --path=/var/www/html --dbname=wordpress --dbuser=wpuser --dbpass=Strattic007"
					/usr/bin/docker exec -it ${client} sh -c "/usr/local/bin/wp --allow-root core install --path=/var/www/html --url=${client}.stratticstage.com --title=${Client} --admin_user=stratticdev --admin_email=support@strattic.com > /root/wpAdminPass"
					/usr/bin/docker exec -it ${client} sh -c "/usr/local/bin/wp --allow-root --path=/var/www/html plugin activate strattic"
					/usr/bin/docker exec -it ${client} sh -c "/bin/chown -R www-data: /var/www/html/"
					/usr/bin/docker cp ${client}:/root/wpAdminPass ${client}.wpAdminPass

					#echo `cat $client.wpAdminPass`
					WPAdminPass=`head -n1 $client.wpAdminPass | awk {'print $3'}`
					echo "UPDATE table set WPAdminPass='${WPAdminPass}' WHERE "
					'''
					dockercmds = ['sh -c "/usr/local/bin/wp --allow-root core download --path=/var/www/html"','sh -c "/usr/local/bin/wp --allow-root config create --path=/var/www/html --dbname=wordpress --dbuser=wpuser --dbpass=Strattic007"','sh -c "/usr/local/bin/wp --allow-root core install --path=/var/www/html --url='+clientname+'.stratticstage.com --title='+clientname.capitalize()+' --admin_user=stratticdev --admin_email=support@strattic.com > /root/wpAdminPass"','sh -c "/usr/local/bin/wp --allow-root --path=/var/www/html plugin activate strattic"','sh -c "/bin/chown -R www-data: /var/www/html/"']
					dcont = docker_conn.container.get(clientname)
					resp = []
					for cmd in dockercmds:
						resp.append(dcont.exec_run(cmd,tty=True,detach=True))

				except Exception as e:
					print(e)
					exit()

				try:

					'''
					#
					# Restart nginx
					#
					echo "Restarting nginx"
					sudo service nginx reload
					'''
					resp = subprocess.Popen(["sudo service nginx reload"],stdout=subprocess.PIPE,stderr=subprocess.STDOUT,shell=True)

				except Exception as e:
					print(e)
					exit()

				try:
					'''
					echo "Provisioning complete for ${client}"

					#send an email
					WPPW=`cat $client.wpAdminPass`
					msg="Provisioning for ${client}.com is complete. Container ID is $dockercontainer. Generated server password is ${genpass}. ${WPPW}"
					JSON=`jq -n --arg email "${supportemail}" --arg domain "$client" --arg msg "$msg" '{ msg: $msg, domain: $domain, email: $email | split(",") }'`
					curl -k -H "Content-Type: application/json" -X POST -d "${JSON}" https://1mtlmyvc77.execute-api.us-east-1.amazonaws.com/PublishNotification

					'''
				except Exception as e:
					print(e)
					exit()


	except Exception as e:
		print(e)
		exit()

if __name__ == "main":
	main()
